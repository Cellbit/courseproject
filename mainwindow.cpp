#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    subLayout = new QCPLayoutGrid;
    ui->widget->plotLayout()->addElement(0, 1, subLayout);
    subLayout->addElement(0, 0, new QCPLayoutElement);
    subLayout->addElement(1, 0, ui->widget->legend);
    subLayout->addElement(2, 0, new QCPLayoutElement);
    ui->widget->plotLayout()->setColumnStretchFactor(1, 0.001);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
        QVector<double> x1, x2, x3, v1, v2, v3, q1, i1, i2, l1, t; //Массивы координат точек
        formVector(x1, x2, x3, v1, v2, v3, q1, i1, i2, l1, t); //формируем вектора вывода функций

        for (int i = 0; i < i2.size(); i++)
            i2[i] *= R2;

        ui->widget->clearGraphs();//Если нужно, но очищаем все графики

        //Говорим, что отрисовать нужно график по нашим двум массивам
        ui->widget->addGraph()->setData(t, x2);
        ui->widget->addGraph()->setData(t, i2);

        //задаем цвета линий и их ширину
        ui->widget->graph(0)->setPen(QPen(Qt::red, 4));
        ui->widget->graph(1)->setPen(QPen(Qt::blue, 4));

        //задаем названия линий
        ui->widget->graph(0)->setName("x2");
        ui->widget->graph(1)->setName("U2");

        ui->widget->legend->setVisible(true); //устанавливаем легенду
        setBorder(x2, i2); //размечаем координатную плоскость
        ui->widget->replot(); //И перерисуем график на нашем widget
}

//формирование векторов для каждой координаты
void MainWindow::formVector(QVector <double>& x1, QVector <double>& x2, QVector <double>& x3, QVector <double>& v1, QVector <double>& v2,
                            QVector <double>& v3, QVector <double>& q1, QVector <double>& i1, QVector <double>& i2, QVector <double>& l1,
                            QVector <double>& t)
{
    double t0 = ui->doubleSpinBox_7->value(); //начало интервала
    double tk = ui->doubleSpinBox_8->value(); //конец интервала
    double h = ui->doubleSpinBox_9->value(); //шаг

    //считываем начальные условия
    v1.push_back(ui->doubleSpinBox->value());
    v2.push_back(ui->doubleSpinBox_2->value());
    v3.push_back(ui->doubleSpinBox_3->value());
    x1.push_back(ui->doubleSpinBox_4->value());
    x2.push_back(ui->doubleSpinBox_5->value());
    x3.push_back(ui->doubleSpinBox_6->value());
    q1.push_back(ui->doubleSpinBox_10->value());
    i1.push_back(ui->doubleSpinBox_11->value());
    i2.push_back(ui->doubleSpinBox_12->value());
    l1.push_back(0.0);
    t.push_back(t0);

    rungeKuttaMethod(v1, v2, v3, x1, x2, x3, q1, i1, i2, l1, t, t0, tk, h); //находим значения функций методом Рунге-Кутта
}

void MainWindow::setBorder(QVector <double>& x2, QVector <double>& i2)
{
    //Подписываем оси Ox и Oy
    ui->widget->xAxis->setLabel("t");
    ui->widget->yAxis->setLabel("x/U");

    //Установим область, которая будет показываться на графике
    ui->widget->xAxis->setRange(ui->doubleSpinBox_7->value(), ui->doubleSpinBox_8->value());//Для оси Ox

    //Для показа границ по оси Oy сложнее, так как надо по правильному
    //вычислить минимальное и максимальное значение в векторах
    double minY = x2[0], maxY = x2[0];
    for (int i = 1; i < x2.size(); i++)
    {
        if (x2[i]<minY) minY = x2[i];
        if (i2[i]>maxY) maxY = i2[i];

        if (x2[i]<minY) minY = x2[i];
        if (i2[i]>maxY) maxY = i2[i];

    }
    ui->widget->yAxis->setRange(minY, maxY);//Для оси Oy
}

void MainWindow::rungeKuttaMethod(QVector <double>& v1, QVector <double>& v2, QVector <double>& v3, QVector <double>& x1, QVector <double>& x2,
                                  QVector <double>& x3, QVector <double>& q1, QVector <double>& i1, QVector <double>& i2, QVector <double>& l1,
                                  QVector <double>& t, double t0, double tk, double h)
{
    int i = 0;
    for (double point = t0 + h; point <= tk; point += h)//Пробегаем по всем точкам
    {

        //матрица коэффициентов для метода
        /*m[0] - v1, m[1] - v2, m[2] - v3, m[3] - x1, m[4] - x2, m[5] - x3
          m[6] - q1, m[7] - i1, m[8] - i2, m[9] - l1*/
        double m[10][4];

        m[0][0] = dV1(x1[i], x2[i], x3[i]);
        m[1][0] = dV2(x1[i], x2[i]);
        m[2][0] = dV3(x1[i], x3[i]);

        m[0][1] = dV1(x1[i] + h/2 * m[0][0], x2[i] + h/2 * m[1][0], x3[i] + h/2 * m[2][0]);
        m[1][1] = dV2(x1[i] + h/2 * m[0][0], x2[i] + h/2 * m[1][0]);
        m[2][1] = dV3(x1[i] + h/2 * m[0][0], x3[i] + h/2 * m[2][0]);

        m[0][2] = dV1(x1[i] + h/2 * m[0][1], x2[i] + h/2 * m[1][1], x3[i] + h/2 * m[2][1]);
        m[1][2] = dV2(x1[i] + h/2 * m[0][1], x2[i] + h/2 * m[1][1]);
        m[2][2] = dV3(x1[i] + h/2 * m[0][1], x3[i] + h/2 * m[2][1]);

        m[0][3] = dV1(x1[i] + h * m[0][2], x2[i] + h * m[1][2], x3[i] + h * m[2][2]);
        m[1][3] = dV2(x1[i] + h * m[0][2], x2[i] + h * m[1][2]);
        m[2][3] = dV3(x1[i] + h * m[0][2], x3[i] + h * m[2][2]);

        v1.push_back(v1[i] + h/6 * (m[0][0] + 2*m[0][1] + 2*m[0][2] + m[0][3]));
        v2.push_back(v2[i] + h/6 * (m[1][0] + 2*m[1][1] + 2*m[1][2] + m[1][3]));
        v3.push_back(v3[i] + h/6 * (m[2][0] + 2*m[2][1] + 2*m[2][2] + m[2][3]));


        m[3][0] = v1[i];
        m[4][0] = v2[i];
        m[5][0] = v3[i];
        m[6][0] = i1[i];

        m[3][1] = v1[i] + h/2 * m[3][0];
        m[4][1] = v2[i] + h/2 * m[4][0];
        m[5][1] = v3[i] + h/2 * m[5][0];
        m[6][1] = i1[i] + h/2 * m[6][0];

        m[3][2] = v1[i] + h/2 * m[3][1];
        m[4][2] = v2[i] + h/2 * m[4][1];
        m[5][2] = v3[i] + h/2 * m[5][1];
        m[6][2] = i1[i] + h/2 * m[6][1];

        m[3][3] = v1[i] + h * m[3][2];
        m[4][3] = v2[i] + h * m[4][2];
        m[5][3] = v3[i] + h * m[5][2];
        m[6][3] = i1[i] + h * m[6][2];

        x1.push_back(x1[i] + h/6 * (m[3][0] + 2*m[3][1] + 2*m[3][2] + m[3][3]));
        x2.push_back(x2[i] + h/6 * (m[4][0] + 2*m[4][1] + 2*m[4][2] + m[4][3]));
        x3.push_back(x3[i] + h/6 * (m[5][0] + 2*m[5][1] + 2*m[5][2] + m[5][3]));
        q1.push_back(q1[i] + h/6 * (m[6][0] + 2*m[6][1] + 2*m[6][2] + m[6][3]));

        if (x2[i] < 0) {

            m[9][0] = ALPHA;
            m[9][1] = ALPHA + h/2 * m[9][0];
            m[9][2] = ALPHA + h/2 * m[9][1];
            m[9][3] = ALPHA + h * m[9][2];
            l1.push_back(l1[i] + h/6 * (m[9][0] + 2*m[9][1] + 2*m[9][2] + m[9][3]));
        }
        else {

            m[9][0] = -ALPHA;
            m[9][1] = -ALPHA + h/2 * m[9][0];
            m[9][2] = -ALPHA + h/2 * m[9][1];
            m[9][3] = -ALPHA + h * m[9][2];
            l1.push_back(l1[i] + h/6 * (m[9][0] + 2*m[9][1] + 2*m[9][2] + m[9][3]));
        }

        m[7][0] = di1(x2[i], l1[i], v2[i], i1[i], q1[i], i2[i]);
        m[8][0] = di2(i1[i], i2[i]);

        m[7][1] = di1(x2[i] + h/2 * m[4][0], l1[i] + h/2 * m[9][0], v2[i] + h/2 * m[1][0], i1[i] + h/2 * m[7][0], q1[i] + h/2 * m[6][0], i2[i] + h/2 * m[8][0]);
        m[8][1] = di2(i1[i] + h/2 * m[7][0], i2[i] + h/2 * m[8][0]);

        m[7][2] = di1(x2[i] + h/2 * m[4][1], l1[i] + h/2 * m[9][1], v2[i] + h/2 * m[1][1], i1[i] + h/2 * m[7][1], q1[i] + h/2 * m[6][1], i2[i] + h/2 * m[8][1]);
        m[8][2] = di2(i1[i] + h/2 * m[7][1], i2[i] + h/2 * m[8][1]);

        m[7][3] = di1(x2[i] + h * m[4][2], l1[i] + h * m[9][2], v2[i] + h * m[1][2], i1[i] + h * m[7][2], q1[i] + h * m[6][2], i2[i] + h * m[8][2]);
        m[8][3] = di2(i1[i] + h * m[7][2], i2[i] + h * m[8][2]);

        i1.push_back(i1[i] + h/6 * (m[7][0] + 2*m[7][1] + 2*m[7][2] + m[7][3]));
        i2.push_back(i2[i] + h/6 * (m[8][0] + 2*m[8][1] + 2*m[8][2] + 2*m[8][3]));

        t.push_back(point);
        i++;
    }
}

double MainWindow::dV1(double x1, double x2, double x3) {
    return (-K1 * x1 + K2 * (x2 - x1) + K3 * (x3 - x1)) / M1;
}

double MainWindow::dV2(double x1, double x2) {
    return (-K2 * (x2 - x1)) / M2;
}

double MainWindow::dV3(double x1, double x3) {
    return (-K3 * (x3 - x1)) / M3;
}

double MainWindow::L1(double x2) {
    if (abs(x2) <= 0.05)
        return (L0 - ALPHA * abs(x2));
    else
        return 0.0001;
}

double MainWindow::di1(double x2, double dl1, double v2, double i1, double q1, double i2) {
    return (E - dl1 * v2 * (In + i1) - q1/C1 - R1 * (In + i1) + R2 * (In + i2)) / L1(x2);
}

double MainWindow::di2(double i1, double i2) {
    return (R1 * (i1 - i2) - R2 * i2) / L2;
}
