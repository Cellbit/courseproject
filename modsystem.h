#ifndef MODSYSTEM_H
#define MODSYSTEM_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>

void formVector(QVector <double> x1, QVector <double> x2, QVector <double> x3); //формирование векторов для каждой координаты

#endif // MODSYSTEM_H
