#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"
#include <math.h>
#include <iostream>

//глобальные данные: жесткости пружин и массы грузов,
const double K1 = 10.0;
const double K2 = 5.0;
const double K3 = 15.0;
const double M1 = 5.0;
const double M2 = 7.0;
const double M3 = 1.0;

//хар-ки конденсаторов, катушек и резисторов
const double C1 = 0.01;
const double L0 = 0.05;
const double L2 = 0.05;
const double R1 = 40.0;
const double R2 = 200.0;
const double ALPHA = 1.0;
const double E = 10.0;
const double In = E / (R1 - R2);

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    //формирование векторов для каждой координаты
    void formVector(QVector <double>& x1, QVector <double>& x2, QVector <double>& x3, QVector <double>& v1, QVector <double>& v2, QVector <double>& v3,
                    QVector <double>& q1, QVector <double>& i1, QVector <double>& i2, QVector <double>& l1, QVector <double>& t);

    //подписываем оси и устанавливаем границы
    void setBorder(QVector <double>& x2, QVector <double>& i2);

    //метод Рунге-Кутта 4-ого порядка точности
    void rungeKuttaMethod(QVector <double>& v1, QVector <double>& v2, QVector <double>& v3, QVector <double>& x1, QVector <double>& x2,
                          QVector <double>& x3, QVector <double>& q1, QVector <double>& i1, QVector <double>& i2, QVector <double>& l1,
                          QVector <double>& t, double t0, double tk, double h);

    //функции для вычисления ОДУ
    double dV1(double x1, double x2, double x3);
    double dV2(double x1, double x2);
    double dV3(double x1, double x3);
    double L1(double x2);
    double di1(double x2, double dl1, double v2, double i1, double q1, double i2);
    double di2(double i1, double i2);

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QCPLayoutGrid *subLayout;
};

#endif // MAINWINDOW_H
